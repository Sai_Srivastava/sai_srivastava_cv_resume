$(function(){
//center text 
Chart.pluginService.register({
    beforeDraw: function (chart) {
        if (chart.config.options.elements.center) {
//Get ctx from string
var ctx = chart.chart.ctx;
            //Get options from the center object in options
var centerConfig = chart.config.options.elements.center;
var fontStyle = centerConfig.fontStyle || 'Roboto';
            var txt = centerConfig.text;
var color = centerConfig.color || '#000';
var sidePadding = centerConfig.sidePadding || 20;
var sidePaddingCalculated = (sidePadding) * (chart.innerRadius )
//Start with a base font of 30px
ctx.font = "0.9em " + fontStyle;
            //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
var stringWidth = ctx.measureText(txt).width;
var elementWidth = (chart.innerRadius ) - sidePaddingCalculated;

// Find out how much the font can grow in width.
var widthRatio = elementWidth / stringWidth;
var newFontSize = Math.floor(20 * widthRatio);
var elementHeight = (chart.innerRadius );

// Pick a new font size so it will not be larger than the height of label.
var fontSizeToUse = Math.min(newFontSize, elementHeight);

            //Set font settings to draw it correctly.
ctx.textAlign = 'center';
ctx.textBaseline = 'middle';
var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
ctx.font = fontSizeToUse+"px " + fontStyle;
ctx.fillStyle = color;
//Draw text in center
ctx.fillText(txt, centerX, centerY);
        }
    }
});


    //get the doughnut chart canvas
    var ctx1 = $("#doughnut-chartcanvas-1");
    var ctx2 = $("#doughnut-chartcanvas-2");
    var ctx3 = $("#doughnut-chartcanvas-3");
    var ctx4 = $("#doughnut-chartcanvas-4");
    var ctx5 = $("#doughnut-chartcanvas-5");
    var ctx6 = $("#doughnut-chartcanvas-6");
    var ctx7 = $("#doughnut-chartcanvas-7");
    var ctx8 = $("#doughnut-chartcanvas-8");
    var ctx9 = $("#doughnut-chartcanvas-9");

    //doughnut chart data
    var data1 = {
        
        datasets: [
            {
                
                data: [70, 30],
                backgroundColor: [
                    "#2471A3",
                    "#DCDCDC",
                    
                  
                ],
                borderColor: [
                    "#2471A3",
                    "#DCDCDC",
                    
                   
                ],
                borderWidth: [1, 1]
            }
        ]
    };

    //doughnut chart data
    var data2 = {
        
        datasets: [
            {
                
                data: [80,20],
                backgroundColor: [
                    "#28B463",
                    "#DCDCDC",
                    
                    
                ],
                borderColor: [
                    "#28B463",
                    "#DCDCDC",
                    
                    
                ],
                borderWidth: [1, 1]
            }
        ]
    };

    //doughnut chart data
    var data3 = {
        
        datasets: [
            {
                
                data: [60,40],
                backgroundColor: [
                    "#3498DB",
                    "#DCDCDC",
                    
                ],
                borderColor: [
                    "#3498DB",
                    "#DCDCDC",
                    
                    
                ],
                borderWidth: [1, 1]
            }
        ]
    };

    //doughnut chart data
    var data4 = {
        
        datasets: [
            {
                
                data: [80, 20],
                backgroundColor: [
                    "#2C3E50",
                    "#DCDCDC",
                  
                ],
                borderColor: [
                    "#2C3E50",
                    "#DCDCDC",
                    
                   
                ],
                borderWidth: [1, 1]
            }
        ]
    };

    //doughnut chart data
    var data5 = {
        
        datasets: [
            {
                
                data: [70,30],
                backgroundColor: [
                    "#2471A3",
                    "#DCDCDC",
                    
                ],
                borderColor: [
                    "#2471A3",
                    "#DCDCDC",
                    
                    
                ],
                borderWidth: [1, 1]
            }
        ]
    };

    //doughnut chart data
    var data6 = {
        
        datasets: [
            {
                
                data: [90,10],
                backgroundColor: [
                    "#DC7633  ",
                    "#DCDCDC",
                    
                ],
                borderColor: [
                    "#DC7633  ",
                    "#DCDCDC",
                    
                    
                ],
                borderWidth: [1, 1]
            }
        ]
    };

    //doughnut chart data
    var data7 = {
        
        datasets: [
            {
                
                data: [60,40],
                backgroundColor: [
                    "#E74C3C",
                    "#DCDCDC",
                    
                ],
                borderColor: [
                    "#E74C3C",
                    "#DCDCDC",
                    
                    
                ],
                borderWidth: [1, 1]
            }
        ]
    };

    //doughnut chart data
    var data8 = {
        
        datasets: [
            {
                
                data: [70,30],
                backgroundColor: [
                    "#F1C40F",
                    "#DCDCDC",
                    
                ],
                borderColor: [
                    "#F1C40F",
                    "#DCDCDC",
                    
                    
                ],
                borderWidth: [1, 1]
            }
        ]
    };

    //doughnut chart data
    var data9 = {
        
        datasets: [
            {
                
                data: [50,50],
                backgroundColor: [
                    "#7D3C98",
                    "#DCDCDC",
                    
                ],
                borderColor: [
                    "#7D3C98",
                    "#DCDCDC",
                    
                    
                ],
                borderWidth: [1, 1]
            }
        ]
    };



    //options
    var options1 = {
        responsive: true,
        title: {
            display: true,
            position: "top",
            fontSize: 18,
            fontColor: "#111"
           
        },
        elements: {
            center: {
            text: 'MS Word',
            fontStyle: 'Arial', // Default is Arial
            sidePadding: 20, // Defualt is 20 (as a percentage)
            innerRadius: "70%"
            }
            },

            
        legend: {
            display: true,
            position: "bottom",
            labels: {
                fontColor: "#333",
                fontSize: 16
            }


            
        },
        tooltips: {enabled: false},
        hover: {mode: null},
        cutoutPercentage: 90,
      
        
    };


    var options2 = {
        responsive: true,
        title: {
            display: true,
            position: "top",
            fontSize: 18,
            fontColor: "#111"
           
        },
        elements: {
            center: {
            text: 'MS Excel',
            fontStyle: 'Arial', // Default is Arial
            sidePadding: 20, // Defualt is 20 (as a percentage)
            innerRadius: "70%"
            }
            },

            
        legend: {
            display: true,
            position: "bottom",
            labels: {
                fontColor: "#333",
                fontSize: 16
            }


            
        },
        tooltips: {enabled: false},
        hover: {mode: null},
        cutoutPercentage: 90,
      
        
    };


    var options3 = {
        responsive: true,
        title: {
            display: true,
            position: "top",
            fontSize: 18,
            fontColor: "#111"
           
        },
        elements: {
            center: {
            text: 'Mulesoft',
            fontStyle: 'Arial', // Default is Arial
            sidePadding: 20, // Defualt is 20 (as a percentage)
            innerRadius: "70%"
            }
            },

            
        legend: {
            display: true,
            position: "bottom",
            labels: {
                fontColor: "#333",
                fontSize: 16
            }


            
        },
        tooltips: {enabled: false},
        hover: {mode: null},
        cutoutPercentage: 90,
      
        
    };


    var options4 = {
        responsive: true,
        title: {
            display: true,
            position: "top",
            fontSize: 18,
            fontColor: "#111"
           
        },
        elements: {
            center: {
            text: 'English',
            fontStyle: 'Arial', // Default is Arial
            sidePadding: 20, // Defualt is 20 (as a percentage)
            innerRadius: "70%"
            }
            },

            
        legend: {
            display: true,
            position: "bottom",
            labels: {
                fontColor: "#333",
                fontSize: 16
            }


            
        },
        tooltips: {enabled: false},
        hover: {mode: null},
        cutoutPercentage: 90,
      
        
    };


    var options5 = {
        responsive: true,
        title: {
            display: true,
            position: "top",
            fontSize: 18,
            fontColor: "#111"
           
        },
        elements: {
            center: {
            text: 'Hindi',
            fontStyle: 'Arial', // Default is Arial
            sidePadding: 20, // Defualt is 20 (as a percentage)
            innerRadius: "70%"
            }
            },

            
        legend: {
            display: true,
            position: "bottom",
            labels: {
                fontColor: "#333",
                fontSize: 16
            }


            
        },
        tooltips: {enabled: false},
        hover: {mode: null},
        cutoutPercentage: 90,
      
        
    };

    var options6 = {
        responsive: true,
        title: {
            display: true,
            position: "top",
            fontSize: 18,
            fontColor: "#111"
           
        },
        elements: {
            center: {
            text: 'Telugu',
            fontStyle: 'Arial', // Default is Arial
            sidePadding: 20, // Defualt is 20 (as a percentage)
            innerRadius: "70%"
            }
            },

            
        legend: {
            display: true,
            position: "bottom",
            labels: {
                fontColor: "#333",
                fontSize: 16
            }


            
        },
        tooltips: {enabled: false},
        hover: {mode: null},
        cutoutPercentage: 90,
      
        
    };

    var options7 = {
        responsive: true,
        title: {
            display: true,
            position: "top",
            fontSize: 18,
            fontColor: "#111"
           
        },
        elements: {
            center: {
            text: 'JAVA/Python',
            fontStyle: 'Arial', // Default is Arial
            sidePadding: 20, // Defualt is 20 (as a percentage)
            innerRadius: "70%"
            }
            },

            
        legend: {
            display: true,
            position: "bottom",
            labels: {
                fontColor: "#333",
                fontSize: 16
            }


            
        },
        tooltips: {enabled: false},
        hover: {mode: null},
        cutoutPercentage: 90,
       
      
        
    };

    var options8 = {
        responsive: true,
        title: {
            display: true,
            position: "top",
            fontSize: 18,
            fontColor: "#111"
           
        },
        elements: {
            center: {
            text: 'Machine learning',
            fontStyle: 'Arial', // Default is Arial
            sidePadding: 20, // Defualt is 20 (as a percentage)
            innerRadius: "70%"
            }
            },

            
        legend: {
            display: true,
            position: "bottom",
            labels: {
                fontColor: "#333",
                fontSize: 16
            }


            
        },
        tooltips: {enabled: false},
        hover: {mode: null},
        cutoutPercentage: 90,
      
        
    };

    var options9 = {
        responsive: true,
        title: {
            display: true,
            position: "top",
            fontSize: 18,
            fontColor: "#111"
           
        },
        elements: {
            center: {
            text: 'HTML/CSS',
            fontStyle: 'Arial', // Default is Arial
            sidePadding: 20, // Defualt is 20 (as a percentage)
            innerRadius: "70%"
            }
            },

            
        legend: {
            display: true,
            position: "bottom",
            labels: {
                fontColor: "#333",
                fontSize: 16
            }


            
        },
        tooltips: {enabled: false},
        hover: {mode: null},
        cutoutPercentage: 90,
      
        
    };


    

    //create Chart class object
    var chart1 = new Chart(ctx1, {
        type: "doughnut",
        data: data1,
        options: options1
    });

    //create Chart class object
    var chart2 = new Chart(ctx2, {
        type: "doughnut",
        data: data2,
        options: options2
    });


    var chart3 = new Chart(ctx3, {
        type: "doughnut",
        data: data3,
        options: options3
    });
    var chart4 = new Chart(ctx4, {
        type: "doughnut",
        data: data4,
        options: options4
    });
    var chart5 = new Chart(ctx5, {
        type: "doughnut",
        data: data5,
        options: options5
    });
    var chart6 = new Chart(ctx6, {
        type: "doughnut",
        data: data6,
        options: options6
    });
    var chart7 = new Chart(ctx7, {
        type: "doughnut",
        data: data7,
        options: options7
    });
    var chart8 = new Chart(ctx8, {
        type: "doughnut",
        data: data8,
        options: options8
    });
    var chart9 = new Chart(ctx9, {
        type: "doughnut",
        data: data9,
        options: options9
    });
});